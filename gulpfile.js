var gulp = require('gulp')
const eslint = require('gulp-eslint')
var mocha = require('gulp-mocha')
// var exec = require('child_process').exec
const path = require('path')
var editFiles = [
    'src/goatstone/**/*.js',
    'gulpfile.js',
    'test/**/*.js'
]
gulp.task('default', ['watchfiles', 'node-serve'])
// gulp.task('default', ['browser-sync', 'lint', 'watchfiles', 'test', 'node-serve'])
gulp.task('watchfiles', function () {
    gulp.watch(editFiles, ['lint', 'test'])
})
gulp.task('travis', ['lint', 'test'], function () {
    console.log('travis task')
})
gulp.task('node-serve', function () {
    const repositoryBasePath = path.join(__dirname)
    var Server = require('./src/goatstone/server/one.js')
    var s = new Server(repositoryBasePath)
    s.start()
})
gulp.task('browser-sync', function () {
    // TODO import library
    // const cmd = path.join(__dirname, '/node_modules/browser-sync/bin/browser-sync.js') +
    // ' start -f ' + path.join(__dirname, 'dist/js')
    // exec(cmd,
    //     (error, stdout, stderr) => {
    //         console.log(`stdout: ${stdout}`)
    //         console.log(`stderr: ${stderr}`)
    //         if (error !== null) {
    //             console.log(`exec error: ${error}`)
    //         }
    //     })
})
gulp.task('lint', function () {
    return gulp
    .src(editFiles)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
})
gulp.task('test', function () {
    const testFiles = [
        'test/setup.js',
        'test/main.js',
        'test/control.test.js',
        'test/list-make.test.js',
        'test/main-component.test.js'
    ]
    return gulp
        .src(testFiles)
        .pipe(mocha())
})
