let expect = require('chai').expect
let OctogoatService = require('goatstone/octogoat-service')
let config = require('../src/config')

describe('OctogoatService', function () {
    let os
    beforeEach(() => {
        os = new OctogoatService(config)
    })
    describe('Getting Data', () => {
        it('should get and return data from backend service', (done) => {
            os.getData('abc')
            .then(function (response) {
                console.log('result', response.data.results[0].snippet)
                done()
            })
            expect(1).to.equal(1)
        })
    })
})
