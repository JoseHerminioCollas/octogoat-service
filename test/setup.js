require('../src/setup') // setup???
const jsdom = require('jsdom')
const { JSDOM } = jsdom

// var exposedProperties = ['window', 'navigator', 'document']

global.document = new JSDOM(`<!DOCTYPE html><p>Hello world</p>`)
global.window = document.defaultView
// Object.keys(document.defaultView).forEach((property) => {
//     if (typeof global[property] === 'undefined') {
//         exposedProperties.push(property)
//         global[property] = document.defaultView[property]
//     }
// })

global.navigator = {
    userAgent: 'node.js'
}
