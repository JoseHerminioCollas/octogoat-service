var express = require('express')
var app = express()
var ip = require('ip')
var expressWinston = require('express-winston')
var winston = require('winston')
require('winston-mongodb').MongoDB
require('../../setup')
var cors = require('cors')
let config = require('../../config')
let OctogoatService = require('goatstone/octogoat-service')

class ServerOne {
    constructor () {
        this.server = {}
        this.port = 5000
        this.ipAdrress = ip.address()

        app.use(expressWinston.logger({
        transports: [
            new(winston.transports.MongoDB)({
                db : 'mongodb://localhost:27017/octogoat',
                collection: 'log'
            })
        ]
        }));
        var router = express.Router()
        router.use(cors())
        router.use((req, res, next) => {
            next()
        })
        router.get('/categories/search', (req, res, next) => {
            res.json({ message: '/categories/search' })
            next()
        })
        router.get('/categories/get_multi', (req, res, next) => {
            res.json({ message: '/categories/get_multi' })
            next()
        })
        router.get('/part/get_multi', (req, res, next) => {
            res.json({ message: '/part/get_multi' })
            next()
        })
        router.get('/part/search', (req, res, next) => {
            let query = req.query.q || ''
	    let start = req.query.start || ''
            this.partSearch(query, start)
            .then((response) => {
                let JSONdata = this.partSearchData(response)
                res.json({ data: JSONdata })
            })
        })
        app.use('/', router)
    }
    start () {
        this.server = app.listen(this.port, this.ipAdrress, () => {
            var host = this.server.address().address
            var port = this.server.address().port
            console.log('goatstone server listening at http://%s:%s', host, port)
        })
    }
    partSearchData (octopartResponse) {
	let hits = octopartResponse.data.hits
	let q = octopartResponse.data.request.q
	let start = octopartResponse.data.request.start
	let returnPack = []
	returnPack.push([hits, q, start])
	returnPack[1] = octopartResponse.data.results
            .map((x) =>
		 {
		     return[
			 x.snippet,
			 [x.item.manufacturer.name, x.item.manufacturer.homepage_url],
			 x.item.specs,
			 x.item.imagesets.map(img => {
			     return [img.credit_string, img.credit_url, img.small_image.url]
			 })
		     ]
		 }
		)
	return returnPack
    }
    partSearch (query, start) {
        // TODO use a stream here
        let os = new OctogoatService(config)
        return os.partSearch(query, start)
    }
    stop () {
        this.server.close()
    }
}
// start the server
new ServerOne().start()

module.exports = ServerOne
