let axios = require('axios')
require('setup')

class OctogoatService {
    constructor (config) {
        this.OCTOPART_API_KEY = config.OCTOPART_API_KEY
        this.args = {
            params: {
                apikey: this.OCTOPART_API_KEY,
                start: 0,
                limit: 10,
                include: [
                    "datasheets", 
                    "short_description", 
                    "descriptions",
                    "imagesets",
                    "specs"
                    ]
            }
        }
        this.baseUrl = 'http://octopart.com/api/v3/'
    }
    partSearch (query, start) {
        const apiPath = 'parts/search'
        let u = this.getURL(apiPath)
        Object.assign(this.args.params, {q: query, start: start})
        return axios.get(u, this.args)
    }
    getURL (apiPath) {
        return `${this.baseUrl}${apiPath}`
    }
}

module.exports = OctogoatService
